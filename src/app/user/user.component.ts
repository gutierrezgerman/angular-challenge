import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {UserServiceService} from '../services/user-service.service';
import {DetailResponse} from '../interfaces/detail-response';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  login: any;
  user: DetailResponse|null;

  constructor(private route: ActivatedRoute,
    private userService: UserServiceService
    ) {
    this.user= null;
    this.login =  this.route.snapshot.paramMap.get('login');
    
   }

  ngOnInit(): void {
    this.getUser();
  }

  getUser(){
    this.userService.getUser(this.login).subscribe(user => {
console.log(user);
      this.user = user;
    } );
  }

}
