import { Injectable } from '@angular/core';
import {environment} from '../../environments/environment';
import { UserResponse} from '../interfaces/user-response';
import { DetailResponse} from '../interfaces/detail-response';
import { HttpClient } from '@angular/common/http'; 
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserServiceService {
  endpointUrl=  environment.apiURL + "/users";
  options: object;
  constructor(
    private http: HttpClient,
  ) { 
    this.options= {      
      observe: 'body',      
      reportProgress: false,
      responseType: 'json',      
    }

  }
  

  
  getUser(username: string): Observable<DetailResponse>{
    
    return this.http.get<DetailResponse>(this.endpointUrl+"/"+username, this.options);
  
      
  }

  getDatosTop5Users(): Observable<UserResponse[]>{
    
    return this.http.get<UserResponse[]>(this.endpointUrl+"?per_page=5", this.options);
  
      
  }
}
