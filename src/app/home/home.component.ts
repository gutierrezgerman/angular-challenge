import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import {UserServiceService} from '../services/user-service.service';
import {UserResponse} from '../interfaces/user-response';

@Component({
  selector: 'app-root',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  title = 'angular-challenge';
  users: UserResponse[];
  constructor(
    
    private router: Router,
    
    private userService: UserServiceService
    ) { 
      this.users=[];
    }

    ngOnInit(): void {
      this.getTopUsers();
      
    }
    getTopUsers(){
      this.userService.getDatosTop5Users().subscribe(users => {

        this.users = users
      } );
    }
    gotouser(login:string){
      console.log(login)
      this.router.navigate(['users/'+login]);
    }
}
